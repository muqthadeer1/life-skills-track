# Service-Oriented Architecture (SOA)

Service-Oriented Architecture (SOA) is a stage in the evolution of application development and/or integration. It defines a way to make software components reusable using interfaces.

Formally, SOA is an architectural approach in which applications make use of services available in the network. In this architecture, services are provided to form applications through a network call over the internet. It utilizes common communication standards to speed up and streamline service integrations in applications. Each service in SOA represents a complete business function.

The key characteristics of SOA include:

- **Reusability:** SOA promotes the reuse of software components through well-defined interfaces.
- **Network-Based:** Services are accessed over a network, often through internet-based communication.
- **Business Functionality:** Each service encapsulates a complete business function.
- **Standardization:** Common communication standards are employed to facilitate service integrations.

Services in SOA are published in a way that makes it easy for developers to assemble their applications using these services. It's important to note that SOA is distinct from microservices architecture.

# Service-Oriented Architecture (SOA)

- **Combining Facilities:**
  SOA enables users to combine a variety of features from existing services to create applications. It's like putting together different tools to build something new.

- **Design Principles:**
  SOA follows a set of design principles that help organize system development. These principles facilitate the integration of components, creating a coherent and decentralized system. Think of it like putting together pieces of a puzzle to form a complete picture.

- **Interoperable Services:**
  SOA-based computing organizes functionalities into a set of interoperable services. These services can seamlessly integrate into different software systems across separate business domains. Imagine these services as interchangeable parts that can be used in various machines.

SOA simplifies the process of building applications by allowing the combination of existing services and providing a structured approach to system development.
# Characteristics of SOA:

- **Interoperability:**
  - Provides interoperability between services.

- **Service Lifecycle Features:**
  - Provides methods for service encapsulation, service discovery, service composition, service reusability, and service integration.

- **Quality of Services (QoS):**
  - Facilitates QoS through service contracts based on Service Level Agreement (SLA).

- **Loose Coupling:**
  - Provides loosely coupled services.

- **Location Transparency:**
  - Provides location transparency with better scalability and availability.

- **Ease of Maintenance and Cost Reduction:**
  - Offers ease of maintenance with reduced costs in application development and deployment.

# Roles in Service-oriented Architecture (SOA):

### Service Provider:
The **service provider** is the maintainer of the service and the organization that makes one or more services available for others to use. To advertise services, the provider can publish them in a registry, together with a service contract that specifies the nature of the service, how to use it, the requirements for the service, and the fees charged.

### Service Consumer:
The **service consumer** can locate the service metadata in the registry and develop the required client components to bind and use the service.

These roles play a crucial part in the SOA framework, where service providers offer services, and service consumers utilize these services by understanding their contracts and requirements.

## Guiding Principles of SOA: 

Standardized service contract: Specified through one or more service description documents.
- **Loose coupling:** Services are designed as self-contained components, maintain relationships that minimize dependencies on other services.
- **Abstraction:** A service is completely defined by service contracts and description documents. They hide their logic, which is encapsulated within their implementation.


 - **Reusability:** Designed as components, services can be reused more effectively, thus reducing development time and the associated costs.
- **Autonomy:** Services have control over the logic they encapsulate and, from a service consumer point of view, there is no need to know about their implementation.
- **Discoverability:** Services are defined by description documents that constitute supplemental metadata through which they can be effectively discovered. Service discovery provides an effective means for utilizing third-party resources.
- **Composability:** Using services as building blocks, sophisticated and complex operations can be implemented. Service orchestration and choreography provide a solid support for composing services and achieving business goals.

## Advantages of SOA: 

- **Service reusability:** In SOA, applications are made from existing services. Thus, services can be reused to make many applications.
_**Easy maintenance:** As services are independent of each other they can be updated and modified easily without affecting other services.
- **Platform independent:** SOA allows making a complex application by combining services picked from different sources, independent of the platform.
- **Availability:** SOA facilities are easily available to anyone on request.
Reliability: SOA applications are more reliable because it is easy to debug small services rather than huge codes
- **Scalability:** Services can run on different servers within an environment, this increases scalability

## Disadvantages of SOA: 

- **High overhead:** A validation of input parameters of services is done whenever services interact this decreases performance as it increases load and response time.
- **High investment:** A huge initial investment is required for SOA.
Complex service management: When services interact they exchange messages to tasks. the number of messages may go in millions. It becomes a cumbersome task to handle a large number of messages.

## Practical applications of SOA: 
SOA is used in many ways around us whether it is mentioned or not. 

- SOA infrastructure is used by many armies and air forces to deploy situational awareness systems.
- SOA is used to improve healthcare delivery.
- Nowadays many apps are games and they use inbuilt functions to run. For example, an app might need GPS so it uses the inbuilt GPS functions of the device. This is SOA in mobile solutions.
- SOA helps maintain museums a virtualized storage pool for their information and content.

### References:
- https://www.geeksforgeeks.org/service-oriented-architecture/

- https://www.youtube.com/watch?v=jL1oVENiYT8&list=PLjVADrCgi-QBMbhFgotpNOT56Vr69X_YO



