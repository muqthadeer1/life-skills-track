# Listening and Active Communication:

**Q1. What are the steps/strategies to do Active Listening?**
 **There Are Few Steps Involved:**
- Give full attention and eliminate distractions.
- Show nonverbal cues like nodding and maintaining eye contact.
- Paraphrase and repeat key points to confirm understanding.
- Ask open-ended questions to encourage further expression.
- Provide constructive feedback and acknowledge emotions.
- Defer judgment, suspend personal opinions, and focus on the speaker's perspective.

**Q2.According to Fisher's model, what are the key points of Reflective Listening?**
1. **Repeating:**
   - Repeat what the person said using your own words. It shows them that you're really paying attention.

2. **Paraphrasing:**
   - Say the same thing in a different way. It helps to make sure you understood their message correctly.

3. **Reflecting Feelings:**
   - Recognize and talk about the emotions the person is sharing. It shows that you understand how they're feeling.

4. **Summarizing:**
   - Give a short summary of what the person said. It helps to wrap up the conversation and make sure you got the main points.


**Q3 What are the obstacles in your listening process?**

**Distractions:**
- External factors like noise, personal devices can divert attention and hinder effective listening.

**Cultural and Language Barriers:**

- Differences in language or cultural nuances may lead to misunderstandings or misinterpretations.

**Assumption and Jumping to Conclusions:**

- Assuming you know what the speaker will say or jumping to conclusions before they finish can lead to misunderstandings.


**Q4.What can you do to improve your listening?**

**Practice Mindfulness:**
- Be present in the moment, focusing on the speaker without letting your mind wander. Mindfulness exercises can help train your attention.

- Create a conducive environment for listening by minimizing external distractions. Turn off unnecessary devices, find a quiet space, and make a conscious effort to focus on the speaker.
- If language is a barrier, ask for clarification, use gestures, seek additional context, or learn basic phrases to enhance understanding.

**Q5.When do you switch to Passive communication style in your day to day life?**

- Individuals might switch to a passive communication style in their day-to-day lives when they want to avoid confrontation, maintain a peaceful environment, or feel uncomfortable expressing their opinions assertively. 
- This might happen in situations where one prioritizes harmony over direct confrontation or wishes to comply with others to keep the atmosphere calm.

**Q6. When do you switch into Aggressive communication styles in your day to day life?**

- This style could manifest when you prioritize your needs over others or believe forceful communication is necessary to achieve your goals, often stemming from feelings of threat or frustration.
- It's important to be mindful of this approach as excessive aggression can strain relationships and hinder effective.

**Q7.When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**
-  it might be because you're indirectly expressing frustration or disagreement. 
- This way of communicating is called passive-aggressive.
 - it happens when someone has difficulty openly expressing their feelings or concerns. 
 - While it might seem like a way to avoid direct conflict, it can create misunderstandings and harm relationships.
 - It's helpful to be aware of these behaviors for more effective and positive communication.

**Q8.How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?**

**Maintain Eye Contact:**

- Establishing and maintaining eye contact can convey confidence and sincerity, reinforcing the assertive message.

**Practice Active Listening:**

- Demonstrate that you value others' opinions by actively listening to their perspective. This helps build mutual respect.

**Be Clear and Specific:**

- Clearly state what you want or need, providing specific details. Ambiguity can lead to misunderstandings.

**Practice Empathy:**

- Acknowledge others' perspectives and feelings. Empathy fosters a more open and understanding communication environment.
