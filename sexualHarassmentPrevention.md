# Sexual Harassment And Preventions

## Definition

Sexual harassment is defined as unwelcome sexual advances, requests for sexual favors, and other verbal or physical harassment of a sexual nature in the workplace or learning environment. This definition is in accordance with the guidelines provided by the Equal Employment Opportunity Commission (EEOC).

## Forms of Sexual Harassment

- **Unwelcome Sexual Advances:** Any unwanted, inappropriate, or offensive sexual propositions or overtures.
  
- **Requests for Sexual Favors:** Unwanted requests for sexual activities, which can create a hostile or uncomfortable environment.

- **Verbal or Physical Harassment:** Inappropriate comments, gestures, or actions of a sexual nature that cause discomfort or distress.

## Broad Scope

Sexual harassment is not limited to explicit sexual behavior or direct targeting of an individual. It can also manifest in broader forms, such as:

- **Negative Comments About a Group:** Negative comments, jokes, or remarks about a particular gender, for example, may constitute a form of sexual harassment.
# Prevention of Sexual Harassment

The most effective weapon against sexual harassment is prevention. Harassment does not disappear on its own. In fact, it is more likely that when the problem is not addressed, the harassment will worsen and become more difficult to remedy as time goes on.

## Employer Responsibilities

- The burden of preventing sexual harassment rests on the employer. In the United States, Canada, and in some European Union Member States, employers are responsible for providing their employees with a work environment that does not discriminate and is free of harassment. Employers are required by law to take steps to prevent and deal with harassment in the workplace. If the employer has not taken all reasonable steps to prevent and deal with harassment in the workplace, they may be liable for any harassment that occurs, even if unaware that the harassment was taking place.

- Most successful preventive strategies and plans on sexual harassment require the involvement of all those concerned and a clear statement of intent. The statement of intent should reflect a real commitment from all parties concerned to recognize the importance of the fight against sexual harassment in the workplace. This is usually accompanied by the establishment of a written policy.

- Anti-harassment policies explain what harassment is, state that harassment will not be tolerated, and set out how employers and employees should respond to incidents of harassment. These policies should also establish a detailed mechanism by which employees can make complaints when sexual harassment occurs.

- Having an anti-harassment policy does not mean that there will be no harassment complaints. However, having an effective policy and procedures, coupled with anti-harassment training for all staff, will assist in preventing harassment and support individuals who are being harassed to come forward and ensure that the problem is addressed quickly and effectively. In the United States, courts have held that an employer who responds quickly and effectively to a complaint by taking steps to remedy the situation and prevent future harassment will not be liable to the same extent, if at all, as an employer who fails to adopt such steps.

- Below are some measures that employers can take to create a harassment-free workplace, based on guidelines from the British Columbia Human Rights Commission manual Preventing Harassment in the Workplace.

- Make it clear that this is a workplace where harassment will not be tolerated.
- Provide education and information about harassment to all staff on a regular basis. The circulation of information, open communication, and guidance are of particular importance in removing the taboo of silence which often surrounds cases of sexual harassment.
- Develop an anti-harassment policy together with employees, managers, and union representatives.
- Communicate the policy to all employees.
- Ensure that all managers and supervisors understand their responsibility to provide a harassment-free work environment.
- Ensure that all employees understand the policy and procedures for dealing with harassment - new and long-term employees alike - this involves training, information, and education.
- Show you mean it - make sure the policy applies to everyone, including managers and supervisors.
- Promptly investigate and deal with all complaints of harassment.
- Appropriately discipline employees who harass other employees.
- Provide protection and support for employees who feel they are being harassed.
- Take action to eliminate discriminatory jokes, posters, graffiti, emails, and photos at the worksite.
- Monitor and revise the policy and education/information programs regularly to ensure that it is still effective for your workplace.

Employers should provide a mechanism for addressing sexual harassment in a confidential and sensitive manner after a grievance has been filed. A well-constructed and well-implemented plan within an organization may stop inappropriate conduct before it creates a problem for individual employees or the company.

Many model anti-harassment policies, from the United States and Canada, can be used as sample frameworks for creating systems to prevent workplace harassment in other countries.

## Employee Responsibilities

In addition to the employer's responsibility to provide a non-discriminatory and non-violent workplace atmosphere, employees must also assume an active role in the prevention of sexual harassment. Employees should commit to doing the following:


## Legal Considerations

Understanding and addressing sexual harassment is crucial not only for maintaining a healthy and respectful environment but also for compliance with legal regulations. Organizations are encouraged to establish clear policies, provide education, and take proactive measures to prevent and address instances of sexual harassment.











## References:
- https://www.rainn.org/articles/sexual-harassment 
- http://hrlibrary.umn.edu/svaw/harassment/explore/5prevention.htm


